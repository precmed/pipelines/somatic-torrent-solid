class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: torrent_variant_caller_hotspots
baseCommand:
  - variant_caller_pipeline.py
inputs:
  - id: input_bam
    type: File
    inputBinding:
      position: 0
      prefix: '--input-bam'
    label: Input BAM file
    doc: Input BAM file(s) containing aligned reads.
    secondaryFiles:
      - .bai
  - id: reference
    type: File
    inputBinding:
      position: 0
      prefix: '--reference-fasta'
    label: Reference genome
    doc: FASTA file containing reference genome
    secondaryFiles:
      - .fai
  - id: plain_bed_file
    type: File
    inputBinding:
      position: 0
      prefix: '--region-bed'
  - id: parameters
    type: File
    inputBinding:
      position: 0
      prefix: '--parameters-file'
    label: Parameters file
    doc: >-
      JSON file containing variant calling parameters. This file can be obtained
      from https://ampliseq.com for your panel. If not provided, default params
      will be used. for more information about parameters, please visit TVC 4.x
      Parameters Description
  - id: ptrim_bed
    type: File
    inputBinding:
      position: 0
      prefix: '--primer-trim-bed'
    doc: The BED file used for primer trimming
  - id: hotspots_vcf
    type: File
    inputBinding:
      position: 0
      prefix: '--hotspot-vcf'
    doc: Hotspots VCF
outputs:
  - id: vcf
    doc: The final VCF file containing also small variants and indels.
    label: VCF file
    type: File
    outputBinding:
      glob: TSVC_variants.vcf
  - id: small_variants
    doc: VCF file containing only SNVs
    type: File
    outputBinding:
      glob: small_variants.vcf
  - id: indel_assembly
    doc: VCF file containing only small indels
    type: File
    outputBinding:
      glob: indel_assembly.vcf
  - id: small_variants_filtered
    type: File
    outputBinding:
      glob: small_variants_filtered.vcf
label: torrent_variant_caller
hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc
