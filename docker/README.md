This is a Dockerfile for creating the image for the
ion torrent pipeline. It uses the https://hub.docker.com/r/iontorrent/tsbuild/
image to build the 'Analysis' DEB package that is then
installed in a base Ubuntu Bionic image
